#include "cuda_runtime.h"
#include "device_launch_parameters.h"

// S�o as mesmas fun��es originais s� que em CUDA, falta integrar no codigo e mandar os parametros necess�rios.

__global__ void MovimentarMontanhasCuda(Montanha* montanha, int quantidade, int velocidade, int larg_tela)
{
	int i = threadIdx.x;

	if (i < quantidade) {
		montanha[i].X[i - 1] = montanha[i].X[i - 1] + velocidade * ((i * 0.02) + 0.01);

		if (montanha[i].X[i - 1] < -larg_tela / 2) {
			montanha[i].X[i - 1] = montanha[i].X[i - 1] + 2 * larg_tela;
		}
	}
}

__global__ void MovimentarChaoCuda(Chao* chao, int quantidade, int velocidade, int larg_tela)
{
	int i = threadIdx.x;

	if (i < quantidade) {
		chao[i].X = chao[i].X + velocidade;

		if (chao[i].X + chao[i].sprite.Largura / 2.0 < 0) {
			chao[i].X = chao[i].X + chao[i].sprite.Largura * (quantidade);
		}
	}
}

__global__ void MovimentarNuvemCuda(Nuvem* nuvem, int quantidade, int velocidade, int larg_tela)
{
	int i = threadIdx.x;

	if (i < quantidade) {
		nuvem[i].X = nuvem[i].X + velocidade * 0.01;

		if (nuvem[i].X < -23.0) {
			nuvem[i].X = nuvem[i].X + larg_tela + 46.0;
		}
	}
}

__global__ void MovimentarObstaculosCuda(Obstaculo* obstaculo, int quantidade, int velocidade, int obstaculoDaVez, double distanciaAtual)
{
	int i = threadIdx.x;
	int largura

		if (i < quantidade) {

			obstaculo[i].X = obstaculo[i].X + velocidade;
			largura = obstaculo[i].sprite[0]->Largura;

			if (obstaculo[i].X + largura < -10)
			{
				getNextObstaculo(&obstaculo[i], obstaculoDaVez);
				obstaculo[i].X = obstaculo[i].X - distanciaAtual;
			}
		}
}

__global__ void MovimentarDinossaurosCuda(Dinossauros* dinossauros, int quantidade, int velocidade) {

	int i = threadIdx.x;

	if (i < quantidade) {
		if (dinossauros[i].Estado == 3)  /// MUERTO
		{
			dinossauros[i].X = dinossauros[i].X + velocidade;
		}
		else
		{
			if (dinossauros[i].Estado == 0 || dinossauros[i].Estado == 1)
			{
				dinossauros[i].Fitness = dinossauros[i].Fitness + 2.0;
			}
			else
			{
				dinossauros[i].Fitness = dinossauros[i].Fitness + 1.0;
			}
		}
	}
}

// Fun��es originais.

void MovimentarMontanhas()
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			montanha[i].X[j] = montanha[i].X[j] + VELOCIDADE * ((i * 0.02) + 0.01);

			if (montanha[i].X[j] < -LARG_TELA / 2)
			{
				montanha[i].X[j] = montanha[i].X[j] + 2 * LARG_TELA;
			}
		}
	}
}

void MovimentarChao()
{
	for (int i = 0; i < CHAO_QUANTIDADE; i++)
	{
		chao[i].X = chao[i].X + VELOCIDADE;

		if (chao[i].X + chao[i].sprite.Largura / 2.0 < 0)
		{
			chao[i].X = chao[i].X + chao[i].sprite.Largura * (CHAO_QUANTIDADE);
		}
	}
}

void MovimentarNuvem()
{
	for (int i = 0; i < NUVEM_QUANTIDADE; i++)
	{
		nuvem[i].X = nuvem[i].X + VELOCIDADE * 0.01;
		if (nuvem[i].X < -23.0)
		{
			nuvem[i].X = nuvem[i].X + LARG_TELA + 46.0;
		}
	}
}

void MovimentarObstaculos()
{
	int Largura;

	for (int i = 0; i < MAX_OBSTACULOS; i++)
	{
		obstaculo[i].X = obstaculo[i].X + VELOCIDADE;
		Largura = obstaculo[i].sprite[0]->Largura;

		if (obstaculo[i].X + Largura < -10)
		{
			getNextObstaculo(&obstaculo[i], ObstaculoDaVez);
			obstaculo[i].X = obstaculo[i].X - DistanciaAtual;
		}
	}
}

void MovimentarDinossauros()
{
	for (int i = 0; i < QuantidadeDinossauros; i++)
	{
		if (Dinossauros[i].Estado == 3)  /// MUERTO
		{
			Dinossauros[i].X = Dinossauros[i].X + VELOCIDADE;
		}
		else
		{
			if (Dinossauros[i].Estado == 0 || Dinossauros[i].Estado == 1)
			{
				Dinossauros[i].Fitness = Dinossauros[i].Fitness + 2.0;
			}
			else
			{
				Dinossauros[i].Fitness = Dinossauros[i].Fitness + 1.0;
			}
		}
	}
}